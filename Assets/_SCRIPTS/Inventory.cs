﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class Inventory : MonoBehaviour {

	public static Inventory staticInstance;

	public enum ItemEnum{Shoes};

	[Serializable]
	public struct ItemStates
	{
		public string name;
		public int amountOfItems;
		public bool allCollected;
	};

	public ItemStates[] amountOfItems;
	


	// Use this for initialization
	void Start () {

		if (staticInstance == null) {
			staticInstance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}

		amountOfItems = new ItemStates[Enum.GetNames(typeof(ItemEnum)).Length];

		for(int i = 0; i < amountOfItems.Length; i++)
		{
			amountOfItems [i].name = Enum.GetName (typeof(ItemEnum), i);
		}

	}


	public void AddItem(ItemEnum nameOfItem)
	{

		amountOfItems [(int)nameOfItem].amountOfItems++;

		CheckItems (nameOfItem);

	}

	void CheckItems(ItemEnum nameOfItem)
	{
		switch (nameOfItem) 
		{

		case ItemEnum.Shoes:
			if(amountOfItems[(int)ItemEnum.Shoes].amountOfItems >= 5)
			{
				amountOfItems [(int)ItemEnum.Shoes].allCollected = true;
				ShoeLevel_StairScript.staticInstance.PlayerCanLeave ();

			}
			break;

		}
	}


}
