﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SchaukelPferd : MonoBehaviour {

	public float moveForce;

	public float waitTime;

	public bool isWackeling;

	Rigidbody _rigidbody;

	

	// Use this for initialization
	void Start () 
	{

		_rigidbody = GetComponent<Rigidbody> ();

	}

	public void TriggerAction()
	{

		_rigidbody.AddRelativeTorque (0f, 0f, moveForce, ForceMode.Impulse);

		StartCoroutine ("WaitTime");

	}

	IEnumerator WaitTime()
	{
		isWackeling = true;
		yield return new WaitForSeconds (waitTime);
		isWackeling = false;
	}
	

}
