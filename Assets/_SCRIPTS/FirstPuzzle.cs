﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPuzzle : MonoBehaviour {

	public static FirstPuzzle staticInstance;

	public int amountOfShoes;

	public bool[] shoesActivated;
	public bool puzzleSolved;
	// Use this for initialization
	void Start () {

		shoesActivated = new bool[amountOfShoes];

		staticInstance = this;

	}
	
	// Update is called once per frame
	public void ShoeActivated(int sIndex)
	{
		shoesActivated [sIndex] = true;

		if (CheckShoesUntilIndex(sIndex)) 
		{
			puzzleSolved = CheckPuzzle ();

			if(puzzleSolved)
			{
				TriggerActionWhenSolved ();
			}
		} else {
			//REST PUZZLE!!
			ResetShoes();
		}

	}

	void ResetShoes()
	{
		for (int i = 0; i < shoesActivated.Length; i++) 
		{
			shoesActivated [i] = false;
		}
	}


	void TriggerActionWhenSolved()
	{

		SceneHandler.staticInstance.ChangeScene ("Shoes");

		//UnityEngine.SceneManagement.SceneManager.LoadScene ("Shoes");
	}


	bool CheckPuzzle()
	{

		bool isSolved = true;

		for (int i = 0; i < shoesActivated.Length; i++) 
		{
			if (shoesActivated [i] == false) 
			{
				isSolved = false;
			}
		}

		return isSolved;
	}

	bool CheckShoesUntilIndex(int sIndex)
	{
		bool isValid = true;

		for (int i = 0; i < (sIndex+1); i++) 
		{
			if (shoesActivated [i] == false) 
			{
				isValid = false;
			}
		}

		Debug.Log ("ShoePuzzle state is valid: " + isValid);
		return isValid;
	}

}
