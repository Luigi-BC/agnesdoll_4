﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour {




	Animator doorAnimator;

	public Door_Parent.doorState currentState;


	public enum LockedState{Unlocked, Locked};
	public LockedState lockState;


	// Use this for initialization
	void Start () 
	{
		currentState = Door_Parent.doorState.Closed;
		doorAnimator = transform.parent.GetComponent<Animator> ();
	}

	public void UnlockDoor()
	{
		lockState = LockedState.Unlocked;
	}
		

	void TriggerAction()
	{

		if (lockState == LockedState.Unlocked) {
			switch (currentState) {
			case Door_Parent.doorState.Closed:
				doorAnimator.Play ("Door_Opening");
				break;

			case Door_Parent.doorState.Open:
				doorAnimator.Play ("Door_Closing");
				break;

			case Door_Parent.doorState.MovingOpen:
				return;

			case Door_Parent.doorState.MovingClosed:
				return;
			}
		} else 
		{
			UI_Message.staticInstance.DisplayText ("It's locked.");
		}
	}



	public void SwitchState(Door_Parent.doorState newState)
	{

		currentState = newState;

	}



}
