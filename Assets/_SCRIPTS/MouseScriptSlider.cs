﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseScriptSlider : MonoBehaviour {



	//[SerializeField]
	float mouseSensitivity;

	Text mouseSensitivity_Text;

	// Use this for initialization
	void Start () {
		
		mouseSensitivity_Text = transform.GetChild (2).GetComponent<Text> ();

		mouseSensitivity_Text.text = "5";
		mouseSensitivity = 5;
		Player_Movement.mouseSensitivity = mouseSensitivity;

	}

	public void UpdateMouseSensitivity (float sliderValue) {

		mouseSensitivity = sliderValue;
	
		Player_Movement.mouseSensitivity = mouseSensitivity;

		mouseSensitivity_Text.text = mouseSensitivity.ToString ("#.##");

	}

}
