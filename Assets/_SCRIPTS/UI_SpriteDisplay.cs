﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SpriteDisplay : MonoBehaviour {


	Canvas display;
	Image img;

	public static UI_SpriteDisplay staticInstance;

	// Use this for initialization
	void Start () {
		img = transform.GetChild (0).GetComponent<Image> ();
		display = GetComponent<Canvas> ();
		//DisplaySprite (false);

		staticInstance = this;
	}
	
	public void DisplaySprite(bool state, Sprite newSprite = null)
	{
		display.enabled = state;
		img.sprite = newSprite;
		PauseMenu.staticInstance.FreezeGame (state);
	}

	public void GoBack()
	{
		DisplaySprite (false);
	}
}
