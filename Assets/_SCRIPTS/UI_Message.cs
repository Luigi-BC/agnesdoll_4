﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Message : MonoBehaviour {

	static Text textComponent;
	public static UI_Message staticInstance;

	// Use this for initialization
	void Start () {
		textComponent = GetComponent<Text> ();
		staticInstance = this;
	}

	public  void DisplayText(string textToDisplay)
	{
		StopCoroutine ("FadeOutText");

		textComponent.text = textToDisplay;

		StartCoroutine ("FadeOutText");
	}


	public  IEnumerator FadeOutText()
	{
		textComponent.color = new Color (1f, 1f, 1f, 1f);
		
		yield return new WaitForSeconds (2f);
		
		while(textComponent.color.a >0f)
		{
			yield return new WaitForEndOfFrame ();

			textComponent.color = new Color (1f, 1f, 1f, textComponent.color.a - 0.05f);
		}

		textComponent.color = new Color (1f, 1f, 1f, 0f);
		yield return null;
	}
	

}
