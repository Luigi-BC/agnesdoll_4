﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[ExecuteInEditMode]
public class AgnesPath : MonoBehaviour {

	public static AgnesPath staticInstance;

	[Range(0.05f, 1f)]
	public float debugSphereSize;
	public GameObject agnesPrefab;
	public GameObject waypointPrefab;
	public List<Transform> waypointList = new List<Transform>();


	#region RUNTIME BEHAVIOUR

	void Start()
	{
		staticInstance = this;

		if(waypointList.Count > 0)
		{
			GameObject agnesInstance =  (GameObject)Instantiate (agnesPrefab, waypointList [0].transform.position + new Vector3(0f,1f,0f), Quaternion.identity, null);
			agnesInstance.GetComponent<PathBehaviour> ().listOfWayPoints = waypointList;
		}
	}

	#endregion

	#region EDITOR 

	//INSTANTIATE WAYPOINT
	public void ButtonPressed()
	{

		GameObject currentInstance = null;

		//Instantiate
		if (waypointList.Count == 0) {
			currentInstance = (GameObject)Instantiate (waypointPrefab, transform);
		} else {
			currentInstance = (GameObject)Instantiate (waypointPrefab, waypointList [waypointList.Count - 1].position, Quaternion.identity, transform);
		}

		//Add to list
		waypointList.Add (currentInstance.transform);


		//Debug.Log ("Agnes Editor Button Pressed");
	}


	void OnDrawGizmos()
	{
		if (waypointList.Count == 0)
			return;


		if (waypointList.Contains (null)) 
		{
			Refresh ();
		}

		for(int i = 0; i < (waypointList.Count-1); i++) 
		{

			Gizmos.DrawWireSphere (waypointList[i].position, debugSphereSize);
			Debug.DrawLine (waypointList [i].position, waypointList [i + 1].position, Color.cyan);

		}

		Gizmos.DrawWireSphere (waypointList [waypointList.Count-1].position, debugSphereSize);

	}

	public void Refresh()
	{

		waypointList.Clear();

		for (int i = 0; i < transform.childCount; i++) 
		{
			waypointList.Add (transform.GetChild (0));
		}

	}


	public void DeleteLast()
	{
		Transform lastWayPoint = waypointList[waypointList.Count-1];



		waypointList.RemoveAt (waypointList.Count-1);


		DestroyImmediate (lastWayPoint.gameObject);

	}

	public void DeleteAll()
	{
		while (transform.childCount > 0) 
		{
			DestroyImmediate (transform.GetChild (0).gameObject);
		}

		waypointList.Clear ();

		//waypointList = new List<Transform> ();
		Refresh ();

	}

	#endregion

}



