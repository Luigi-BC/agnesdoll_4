﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AgnesPath))]
public class Agnes_Path_Editor : Editor {



	public override void OnInspectorGUI()
	{
		DrawDefaultInspector ();

		AgnesPath pathScript = (AgnesPath)target;
		if(GUILayout.Button("Generate Waypoint"))
		{
			pathScript.ButtonPressed ();
		}


		if (GUILayout.Button ("Clear All Points")) 
		{
			pathScript.DeleteAll ();
		}

		if (GUILayout.Button ("Delete Last Point")) 
		{
			pathScript.DeleteLast ();
		}

		if (GUILayout.Button ("REFRESH")) 
		{
			pathScript.Refresh ();
		}

	}
}
