﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrightnessSettings : MonoBehaviour {
	//[SerializeField]
	Material cameraMat;

	//[SerializeField]
	float brightness;

	Text brightnessUI_Text;

	// Use this for initialization
	void Start () {
		cameraMat = Camera.main.GetComponent<CameraEffect> ().mat;
		brightnessUI_Text = transform.GetChild (2).GetComponent<Text> ();
		brightnessUI_Text.text = "0";
	}
	
	public void UpdateBrightness (float sliderValue) {

		brightness = sliderValue;
		cameraMat.SetFloat("_GameBrightness", brightness);

		brightnessUI_Text.text = (brightness*10).ToString ("#.##");

	}
}
