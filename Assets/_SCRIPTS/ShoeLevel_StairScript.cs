﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoeLevel_StairScript : MonoBehaviour {

	public bool playerCanLeave;

	public static ShoeLevel_StairScript staticInstance;


	void Start()
	{
		staticInstance = this;
	}

	
	public void PlayerCanLeave () {

		playerCanLeave = true;

	}


	
	public void TriggerAction()
	{
		if(playerCanLeave)
		{
			SceneHandler.staticInstance.ChangeScene ("Hub");
		//	UnityEngine.SceneManagement.SceneManager.LoadScene ("Hub");
		}else{
			UI_Message.staticInstance.DisplayText ("You cannot leave yet.");
		}
		

	}
}
