﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Player") 
		{
			GameOver.staticInstance.Death ();
		}
	}
}
