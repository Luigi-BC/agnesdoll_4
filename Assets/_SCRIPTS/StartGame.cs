﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour {

  //  [SerializeField] Button m_myButton;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	public void LoadLevel ()
    {
        SceneManager.LoadScene("Hub");
	}

    public void QuitGame()
    {
        Application.Quit();

    }
}
