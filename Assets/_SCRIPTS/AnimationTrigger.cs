﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class AnimationTrigger : MonoBehaviour {

	Animator anim;

	string animationName;

	[SerializeField]
	bool playOnlyOnce;

	int timesPlayed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider col) {
		if(col.tag == "Player")
		if(playOnlyOnce)
		{
			if(timesPlayed < 1)
			{
				timesPlayed++;
				anim.Play (animationName);
			}
		}
	}

}
