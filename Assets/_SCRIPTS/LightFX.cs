﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFX : MonoBehaviour {

	public float minRange;
	public float maxRange;

//	public float minChangeTime;
//	public float maxChangeTime;

	public float maxMoveDistance;

	public Vector3 startPosition;

	public float minIdleTime;
	public float maxIdleTime;

	[Range(0f,1f)]
	public float lerpSpeed;

	Light lightSource;

	float desiredRangeValue;
	Vector3 desiredPosition;
	// Use this for initialization
	void Start () {
		lightSource = GetComponent<Light> ();
		StartCoroutine ("ChangeRange");
		startPosition = transform.position;
	}


	IEnumerator ChangeRange()
	{

		for (;;) 
		{
			//WAIT-TIME (IDLE STATE)
			float newWaitTime = Random.Range (minIdleTime, maxIdleTime);
			yield return new WaitForSeconds (newWaitTime);

			desiredRangeValue = Random.Range (minRange, maxRange);

			float xPos = Random.Range (startPosition.x, startPosition.x + maxMoveDistance);
			float yPos = Random.Range (startPosition.y, startPosition.y + maxMoveDistance);
			float zPos = Random.Range (startPosition.z, startPosition.z + maxMoveDistance);
			desiredPosition = new Vector3 (xPos, yPos, zPos);
		}
		
	}

	void Update()
	{

		lightSource.range = Mathf.Lerp (lightSource.range, desiredRangeValue, lerpSpeed);

		transform.position = Vector3.Lerp (transform.position, desiredPosition, lerpSpeed);
		
	}
}
