﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour {


	Rigidbody playerRB;

	[SerializeField]
	float walkSpeed;
	[SerializeField]
	float runSpeed;

	[SerializeField]
	public static float mouseSensitivity;
	Vector3 inputVector;

	float xRotation;
	float yRotation;
	public static Transform camTransform;

	[SerializeField]
	float jumpForce;
	[SerializeField]
	float jumpTime;

	bool isJumping;
	[SerializeField]
	float currentMovementSpeed;
	// Use this for initialization
	void Start () {
		playerRB = GetComponent<Rigidbody> ();
		camTransform = transform.GetChild(0).transform;
	}
	
	// Update is called once per frame
	void Update () {

		if (PauseMenu.gameFreeze)
			return;

		if (PauseMenu.pauseMenuActivated) 
		{
			return;
		}

		Mouse ();
		inputVector = GetInput ();

	}

	Vector3 GetInput()
	{
		float xInput = Input.GetAxisRaw ("Horizontal");
		float yInput = Input.GetAxisRaw ("Vertical");

		if (Input.GetKeyDown (KeyCode.Space) && isJumping == false) 
		{
			StartCoroutine ("JumpWait");
			playerRB.AddRelativeForce (new Vector3 (0f, jumpForce, 0f),ForceMode.Impulse);
		}

		if (Input.GetKey (KeyCode.LeftShift)) {
			currentMovementSpeed = runSpeed;
		} else {
			currentMovementSpeed = walkSpeed;
		}


		return new Vector3 (xInput, 0f, yInput);
	}

	IEnumerator JumpWait()
	{
		isJumping = true;
		yield return new WaitForSeconds (jumpTime);
		isJumping = false;
	}


	void Mouse()
	{
		float xInput = Input.GetAxis ("Mouse X");
		float yInput = Input.GetAxis ("Mouse Y");


		xRotation += (xInput * mouseSensitivity);

		yRotation += (yInput * mouseSensitivity);
		yRotation = Mathf.Clamp (yRotation, -90, 90);


		camTransform.localRotation = Quaternion.Euler (-yRotation, xRotation, 0f);



	}


	void FixedUpdate(){

		if (PauseMenu.gameFreeze)
			return;

		Vector3 forceVector = inputVector.normalized * currentMovementSpeed * Time.fixedDeltaTime;

		Quaternion lookRotation = Quaternion.Euler (0f, camTransform.eulerAngles.y, 0f);

		Vector3 rotatedVector = lookRotation * forceVector;

		playerRB.velocity = new Vector3 (rotatedVector.x, playerRB.velocity.y, rotatedVector.z);

	}

}
