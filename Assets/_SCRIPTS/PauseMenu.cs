﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	[SerializeField]
	public static bool pauseMenuActivated;
	public static bool optionsMenuActivated;

	Canvas pauseCanvas;
	Canvas optionsCanvas;

	public static PauseMenu staticInstance;

	public static bool gameFreeze;

	// Use this for initialization
	void Start () 
	{

		staticInstance = this;

		if (SceneManager.GetActiveScene ().name != "StartSzene") {
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = false;
		}
		pauseCanvas = GetComponent<Canvas> ();
		optionsCanvas = GameObject.Find ("OptionsMenu").GetComponent<Canvas> ();
	}

	void Update()
	{
		
		if (Input.GetKeyDown (KeyCode.Escape)) 
		{
			
			if (optionsMenuActivated) {
				SwitchOptionsMenu (false);
			} else {				 
				pauseMenuActivated = !pauseMenuActivated;
				SwitchCanvas (pauseMenuActivated);
			}
		}

		//Debug.Log (Time.timeScale);

	}

	public void SwitchCanvas(bool visibility)
	{
		pauseCanvas.enabled = visibility;
		pauseMenuActivated = visibility;
		FreezeGame (visibility);
	}

	public void FreezeGame(bool state)
	{
		gameFreeze = state;
	//	Debug.Log ("Freezestate: " + state);
		if (state == true) {
			Time.timeScale = 0.0f;
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = true;
		} else {
			Time.timeScale = 1.0f;
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = false;
		}
	}

	

	public void QuitGame()
	{
		Application.Quit ();
	}

	public void BackToMenu()
	{
		SceneManager.LoadScene (0,LoadSceneMode.Single);
	}



	public void SwitchOptionsMenu(bool menuState)
	{

		optionsMenuActivated = menuState;

		optionsCanvas.enabled = menuState;
	}

}
