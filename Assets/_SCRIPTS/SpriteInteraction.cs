﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteInteraction : MonoBehaviour {

	Sprite sprt;

	// Use this for initialization
	void Start () 
	{
		if (GetComponent<SpriteRenderer> () != null) {
			sprt = GetComponent<SpriteRenderer> ().sprite;
		} else {
			
			Texture2D tex = GetComponent<MeshRenderer> ().material.GetTexture ("_MainTex") as Texture2D;
			sprt = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
		}



	}
	
	// Update is called once per frame
	public void TriggerAction () 
	{
		UI_SpriteDisplay.staticInstance.DisplaySprite (true, sprt);
	}
}
