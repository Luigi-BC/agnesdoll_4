﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {


	public Inventory.ItemEnum itemName;


	// Use this for initialization
	void Start () {
		
	}

	public void TriggerAction()
	{
		Inventory.staticInstance.AddItem (itemName);
		Destroy (gameObject);
	}

}
