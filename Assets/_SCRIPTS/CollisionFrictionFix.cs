﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionFrictionFix : MonoBehaviour {



	public PhysicMaterial zeroFrictionMat;

	// Use this for initialization
	void Start () {
	
		Collider[] colliderList = Object.FindObjectsOfType<Collider> ();


		foreach(Collider c in colliderList)
		{

			c.material = zeroFrictionMat;
			
		}

	}
}
