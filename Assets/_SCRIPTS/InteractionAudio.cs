﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class InteractionAudio : MonoBehaviour {

	AudioSource audioSource;

	// Use this for initialization
	void Start () 
	{
		GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	public void TriggerAction() 
	{
		audioSource.Play ();
	}
}
