﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_Information : MonoBehaviour {

	Text uiText;
	CanvasGroup uiCanvas;

	public static UI_Information staticInstance;

	// Use this for initialization
	void Start () {

		staticInstance = this;
		uiCanvas = GetComponent<CanvasGroup> ();
		uiText = transform.GetChild (1).GetComponent<Text> ();

	}

	public void DisplayText(string textToDisplay)
	{
		uiText.text = textToDisplay;
		SetCanvasVisibility (true);
		
	}

	public void SetCanvasVisibility(bool visibility)
	{

		PauseMenu.staticInstance.FreezeGame (visibility);


		float alphaValue = 0f;

		if (visibility == true) 
		{
			alphaValue = 1f;
		}

		uiCanvas.alpha = alphaValue;
		uiCanvas.interactable = visibility;
		

	}

}
