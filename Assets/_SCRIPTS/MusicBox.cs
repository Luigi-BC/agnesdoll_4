﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBox : MonoBehaviour {

	bool musicBoxIsActive;
	[SerializeField]
	float musicTime;
	[SerializeField]
	float timeFuckUp;

	public static MusicBox instance;

	Light musicLight;
	AudioSource musicSource;

	// Use this for initialization
	void Start () {

		instance = this;

		musicLight = transform.GetChild (0).GetComponent<Light> ();
		musicSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.F))
		{
			if (musicBoxIsActive == false) {
				musicBoxIsActive = true;
				StartCoroutine ("PlaySong");
              
			}
		}
	}

	public void DecreaseTime()
	{
		musicTime -= timeFuckUp;
	}

	public IEnumerator PlaySong()
	{
		musicLight.enabled = true;
		musicSource.Play ();
		
		yield return new WaitForSecondsRealtime (musicTime);

		musicLight.enabled = false;
		musicBoxIsActive = false;
		musicSource.Stop ();
        GameOver.TimeIsUp();
    }

    


}
