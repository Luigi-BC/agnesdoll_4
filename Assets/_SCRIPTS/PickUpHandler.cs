﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpHandler : MonoBehaviour {

	public static PickUpHandler staticInstance;


	void Awake()
	{
		staticInstance = this;
	}

	// Use this for initialization
	void Start () {
		
	}

	void Update()
	{
		if (PauseMenu.pauseMenuActivated) 
		{
			return;
		}

		Ray ray = new Ray (Camera.main.transform.position,Player_Movement.camTransform.forward);
		RaycastHit hit;


		if (Physics.Raycast (ray, out hit, 2.3f)) 
		{
			if (hit.collider.tag == "Interactable") 
			{
				PressEScript.SetText (true);
				if (Input.GetKeyDown (KeyCode.E)) 
				{
					hit.transform.SendMessage ("TriggerAction");
					PressEScript.SetText (false);
				}
			} else {
				PressEScript.SetText (false);
			}
		} else {
			PressEScript.SetText (false);
		}


	}



//	void OnCollisionEnter(Collision col)
//	{
//		if (col.gameObject.tag == "PickUp") 
//		{
//
//			
//
//		}
//	}


}
