﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemLieferando : MonoBehaviour {

	public Inventory.ItemEnum itemToDeliver;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void TriggerAction () 
	{
		if (Inventory.staticInstance.amountOfItems [(int)itemToDeliver].allCollected == true) {

			transform.GetChild (0).gameObject.SetActive (true);
			UI_Message.staticInstance.DisplayText ("Du hast es geschafft, die Seelen der Kinder wurden befreit, sie müssen keine angst mehr vor Agnes haben...oder doch?.");
		} else {
			UI_Message.staticInstance.DisplayText ("Sammel zuerst die Schuhe ein und kehre dann zurück.");
		}

	}
}
