﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle_Shoe : MonoBehaviour {

	[SerializeField]
	int shoeIndex;
	[SerializeField]
	float animationDistance;

	Vector3 startPosition;
	/// <summary>
	/// Zero when deactivated. 0,animationDistance,0 when activated
	/// </summary>
	Vector3 distance;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;
	}
	
	public void TriggerAction()
	{
		FirstPuzzle.staticInstance.ShoeActivated (shoeIndex);

	}


	void Update()
	{
		

		if(FirstPuzzle.staticInstance.shoesActivated[shoeIndex] == true)
		{
			distance = new Vector3 (0f, animationDistance,0f);
		}else{
			distance = Vector3.zero;
		}

		transform.position = startPosition + distance;
	}
}
