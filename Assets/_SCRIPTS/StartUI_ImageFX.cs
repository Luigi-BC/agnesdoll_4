﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartUI_ImageFX : MonoBehaviour {


	public float moveSpeed;

	Vector3 startPosition;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 mouseVector = (Vector2)Input.mousePosition - new Vector2 (Screen.width/2, Screen.height/2);


		mouseVector = mouseVector / Screen.width;

		transform.position = startPosition + (Vector3)(mouseVector * moveSpeed);

	}
}
