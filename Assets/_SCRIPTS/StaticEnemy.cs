﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticEnemy : MonoBehaviour {

	public float basicIdleTime;
	public float maxRandomOffsetIdle;

	public float basicAttackTime;
	public float maxRandomOffsetAttack;

	public enum EnemyState{Idle, Searching, PlayerFound};
	public EnemyState currentState;

	public Collider viewCollider;

	public List<Collider> triggersInCollider = new List<Collider> ();



	void OnTriggerEnter(Collider col)
	{
		triggersInCollider.Add (col);
	}


	void OnTriggerExit(Collider col)
	{
		triggersInCollider.Remove (col);
	}


	void Update()
	{
		if (currentState == EnemyState.Searching) {
			foreach (Collider col in triggersInCollider) {
				if (col.tag == "Player") {
					StopAllCoroutines ();
					currentState = EnemyState.PlayerFound;
				}
			}
		}
	}



	// Use this for initialization
	void Start () 
	{
		StartCoroutine ("AttackWaitTimer", 0f);
	}
	

	IEnumerator IdleWaitTimer(float waitTime)
	{
		currentState = EnemyState.Idle;

		yield return new WaitForSeconds(waitTime);

		float newAttackWaitTime = 0f;

		newAttackWaitTime = basicAttackTime + (Random.Range (-maxRandomOffsetAttack, maxRandomOffsetAttack));

		StartCoroutine ("AttackWaitTimer", newAttackWaitTime);

	}


	IEnumerator AttackWaitTimer(float waitTime)
	{

		currentState = EnemyState.Searching;

		yield return new WaitForSeconds (waitTime);

		float newIdleWaittime = 0f;

		newIdleWaittime = basicIdleTime + (Random.Range (-maxRandomOffsetIdle, maxRandomOffsetIdle));

		StartCoroutine ("IdleWaitTimer", newIdleWaittime);

	}


}
