﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door_Parent : MonoBehaviour {


	DoorScript childScript;

	public 	enum doorState{Closed, Open, MovingOpen, MovingClosed};


	// Use this for initialization
	void Start () 
	{
		childScript = GetComponentInChildren<DoorScript> ();
	}


	public void ChangeDoorState(doorState newState)
	{
		childScript.SwitchState (newState);
	}

}
