﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

	[SerializeField]
   
  
	public static GameOver staticInstance;

	// Use this for initialization
	void Start () {
		
		staticInstance = this;
	}

    public void Death()
    {
        //enables canvas to show gameover screen
       
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);

		//PauseMenu.staticInstance.FreezeGame (true);
    }


    // Update is called once per frame
    static public void TimeIsUp ()
    {
               
        //Bildschirm einfrieren
        //GameObject.Find("Player").GetComponent<Player_Movement>().SetCursorLock(false);
        //suche nach einem objekt mit dem tag "player", greife auf den Player_Movement-Script zu und deaktiviere ihn
        GameObject.Find("Player").GetComponent<Player_Movement>().enabled = false;
        
	}
    


   
     public  void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}


}
