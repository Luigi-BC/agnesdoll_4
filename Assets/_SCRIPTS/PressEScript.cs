﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PressEScript : MonoBehaviour {

	static Outline outline;
	static Text _text;

	// Use this for initialization
	void Start () {
		outline = GetComponent<Outline> ();
		_text = GetComponent<Text> ();

		SetText (false);
	}
	
	// Update is called once per frame
	public static void SetText (bool state) {

		outline.enabled = state;
		_text.enabled = state;

	}
}
