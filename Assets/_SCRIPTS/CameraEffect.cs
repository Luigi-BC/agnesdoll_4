﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraEffect : MonoBehaviour {


	public static CameraEffect staticInstance;

	public Material mat;

	void Awake()
	{
		staticInstance = this;
	}

	void Start()
	{

		SceneHandler.staticInstance.SetCameraMaterial (mat);

		if (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().name == "StartSzene") 
		{
			mat.SetFloat ("_Dream", 0f);
		}else{
			SceneHandler.staticInstance.FadeIn ();
		}
	}

	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		Graphics.Blit (src, dest, mat);
	}

}
