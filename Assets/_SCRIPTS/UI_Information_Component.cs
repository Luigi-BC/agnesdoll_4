﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Information_Component : MonoBehaviour {


	public enum DisplayModes {TimedAfterStart, InteractionWithObject}
	public DisplayModes displayMode;

	public float timeAfterStart;
	[Header("FOR INTERACTION SET TAG")]
	[Header("TO 'INTERACTABLE' AND ADD")]
	[Header("A COLLIDER!\n")]

	[SerializeField]
	[TextArea(15,20)]
	string displayMessage;

	// Use this for initialization
	void Start () {
		if (displayMode == DisplayModes.TimedAfterStart) 
		{
			StartCoroutine ("DisplayTimer");
		}
	}

	IEnumerator DisplayTimer()
	{
		yield return new WaitForSeconds (timeAfterStart);
		UI_Information.staticInstance.DisplayText (displayMessage);
	}


	void TriggerAction()
	{
		if (displayMode == DisplayModes.InteractionWithObject) 
		{
			UI_Information.staticInstance.DisplayText (displayMessage);
		}
	}
}


