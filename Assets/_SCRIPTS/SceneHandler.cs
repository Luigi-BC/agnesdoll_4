﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour {


	public Material cameraMAT;
	public static SceneHandler staticInstance;


	void Awake()
	{
		
		if (staticInstance == null) 
		{
			staticInstance = this;
		} else {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}

	// Use this for initialization
	public void SetCameraMaterial (Material camMat) 
	{
		cameraMAT = camMat;
	}
	
	// Update is called once per frame
	public void ChangeScene(string sceneName) 
	{
		StartCoroutine("FadeOutTimer", sceneName);
	}

	public void FadeIn()
	{
		StartCoroutine ("FadeInTimer");
	}

	IEnumerator FadeInTimer()
	{

		float iterator = 1f;
		cameraMAT.SetFloat ("_Dream", iterator);

		yield return new WaitForSeconds (2f);

		while(iterator >0f){
			iterator -= 0.01f;
			cameraMAT.SetFloat ("_Dream", iterator);
			yield return new WaitForEndOfFrame ();
		}

		cameraMAT.SetFloat ("_Dream", 0f);

		yield return null;

	}


	IEnumerator FadeOutTimer(string sceneName)
	{
		float iterator = 0f;

		while(iterator <1f){
			iterator += 0.01f;
			cameraMAT.SetFloat ("_Dream", iterator);
			yield return new WaitForEndOfFrame ();
		}

		cameraMAT.SetFloat ("_Dream", 1f);

		yield return new WaitForSeconds (0.5f);


		SceneManager.LoadScene (sceneName);
		yield return null;

	}

}
