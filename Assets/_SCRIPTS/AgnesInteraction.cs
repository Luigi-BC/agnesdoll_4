﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgnesInteraction : MonoBehaviour {

	[SerializeField]
	string agnesText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void TriggerAction () 
	{
		UI_Message.staticInstance.DisplayText (agnesText);
		GameObject.Find ("DoorToPuzzle").SendMessage ("UnlockDoor");
	}
}
