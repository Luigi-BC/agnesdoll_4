﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class JumpScare_TRIGGER : MonoBehaviour {


	bool alreadyTriggered;

	public JumpScare_Base jsScript;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnTriggerEnter(Collider col) {

		if (col.tag == "Player") 
		{
			if (alreadyTriggered == false) 
			{
				jsScript.TriggerJumpScare ();
				alreadyTriggered = true;
			}
		}

	}
}
