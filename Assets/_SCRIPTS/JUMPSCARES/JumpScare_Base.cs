﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpScare_Base : MonoBehaviour {

	// Use this for initialization
	public virtual void Start () {
		
	}
	
	// Update is called once per frame
	public virtual void Update () {
		
	}

	public virtual void TriggerJumpScare()
	{
		
	}
}
