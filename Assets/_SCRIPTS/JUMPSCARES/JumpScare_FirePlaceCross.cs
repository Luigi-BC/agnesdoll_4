﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpScare_FirePlaceCross : JumpScare_Base {

	public Rigidbody crossRigidBody;

	public float rotationForce;

	// Use this for initialization
	public override void Start () {

		crossRigidBody = GetComponent<Rigidbody> ();
		crossRigidBody.useGravity = false;

	

	}
	
	// Update is called once per frame
	public override void TriggerJumpScare ()
	{
	//	base.TriggerJumpScare ();
	
		crossRigidBody.useGravity = true;
		crossRigidBody.AddTorque (rotationForce, 0f, 0f, ForceMode.Impulse);

	}
}
