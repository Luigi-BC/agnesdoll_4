﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathBehaviour : MonoBehaviour {


	public List<Transform> listOfWayPoints = new List<Transform>();


	[SerializeField] float movementSpeed;



	//float currentMovementSpeed;

	Rigidbody object_RB;

//	Vector3 playerStandingPoint;
//	float halfColliderHeight;

	[SerializeField]
	int nextWayPoint;

	//Add until end is reached, then subtract until start is reached
	[SerializeField]
	int addOrSub = 1;

	Transform lookRotateObject;

	// Use this for initialization
	void Start () {

		lookRotateObject = transform.GetChild (0);
		addOrSub = 1;
//		halfColliderHeight = GetComponent<CapsuleCollider> ().height / 2f;
		nextWayPoint = 1;
		object_RB = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {

		//Calculate Playerstanding point (Lowest Point)
		//playerStandingPoint = transform.position - new Vector3(0f, 0f, halfColliderHeight);

		Vector3 desiredWayPoint = listOfWayPoints [nextWayPoint].position;

		Vector3 desiredDirection = desiredWayPoint - transform.position;

		desiredDirection= new Vector3 (desiredDirection.x, 0f, desiredDirection.z);

//		desiredDirection = desiredDirection.normalized;

		//TIMES SPEED
		Vector3 desiredSpeed = desiredDirection.normalized * movementSpeed;


		object_RB.velocity = new Vector3(desiredSpeed.x, object_RB.velocity.y, desiredSpeed.z);



		//ROTATION
		//transform.rotation = Quaternion.LookRotation(desiredWayPoint);
		//object_RB.rotation = 
		//Debug.Log(Quaternion.LookRotation(desiredWayPoint,Vector3.up).eulerAngles);


		lookRotateObject.LookAt (desiredWayPoint);
		Quaternion planedLookRotation = Quaternion.Euler (0f, lookRotateObject.eulerAngles.y, 0f);
		Quaternion planedRotationSelf = Quaternion.Euler (0f, transform.eulerAngles.y, 0f);
		Quaternion desiredRotation = Quaternion.Lerp (planedRotationSelf, planedLookRotation, 0.3f);

		transform.rotation = desiredRotation;


		Vector3 planedPosition = new Vector3 (transform.position.x, 0f, transform.position.z);
		Vector3 planedWayPointPosition = new Vector3 (desiredWayPoint.x, 0f, desiredWayPoint.z);

		if (Vector3.Distance (planedPosition, planedWayPointPosition) < 0.08f) 
		{
			
			if (nextWayPoint == 0) 
			{
				addOrSub = 1;
			}else if(nextWayPoint == (listOfWayPoints.Count-1))
			{
				addOrSub = -1;
			} 

			nextWayPoint += addOrSub;

		}

		

	}
}
