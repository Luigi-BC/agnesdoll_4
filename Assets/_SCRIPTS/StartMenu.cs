﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour {



	public Canvas optionsCanvas;

	// Use this for initialization
	void Start () {
		
	}

	public void BackFromOptions()
	{
		optionsCanvas.enabled = false;
	}

	public void StartGame()
	{
		SceneHandler.staticInstance.ChangeScene ("Hub");
	}

	public void Credits()
	{
		//CREDITS

	}

	public void Options()
	{
		PauseMenu.staticInstance.SwitchOptionsMenu (true);
	}

	public void QuitGame()
	{
		Application.Quit ();
	}

}
