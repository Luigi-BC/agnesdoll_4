﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(UI_Information_Component))]
public class UI_Information_Editor : PropertyDrawer {

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{


		EditorGUI.BeginProperty (position, label, property);


		position = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), label);


		var indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;


		var textRect = new Rect (position.x, position.y, 90, 200);


		EditorGUI.PropertyField (textRect, property.FindPropertyRelative ("displayMessage"), GUIContent.none);

		EditorGUI.indentLevel = indent;

		EditorGUI.EndProperty ();

	}


}
