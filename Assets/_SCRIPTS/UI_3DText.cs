﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_3DText : MonoBehaviour {

	CanvasGroup canvasGroup;
	ParticleSystem particleSys;
	Color startColor;


	[SerializeField]
	float triggerDistance;

	enum DisplayState {Visible, Invisible, Changing};

	DisplayState currentDisplayState;

	ParticleSystem.MainModule mainMod;
	Vector3 particlePos;

	[SerializeField]
	bool triggerOnlyOnce;

	bool alreadyChecked;

	// Use this for initialization
	void Awake () {
		particleSys = transform.GetChild (0).GetComponent<ParticleSystem> ();
		particlePos = particleSys.transform.position;

		startColor = particleSys.main.startColor.color;
		mainMod = particleSys.main;

		canvasGroup = transform.GetChild(1).GetComponent<CanvasGroup> ();


		//FIRST FADE OUT
		mainMod.startColor = new Color(0,0,0,0);

		canvasGroup.alpha = 0f;

		currentDisplayState = DisplayState.Invisible;


	}
	
	// Update is called once per frame
	void Update () {



	//	Color desiredColor = Color.Lerp (startColor, Color.clear, Mathf.Clamp01((1-playerDistance)*4f));
		float playerDistance = Vector3.Distance(particlePos, Camera.main.transform.position);

	//	Debug.DrawLine (transform.position, Camera.main.transform.position, Color.cyan);

		if(playerDistance <= triggerDistance)
		{
			if(currentDisplayState == DisplayState.Invisible)
			{
				StopCoroutine ("FadeOut");
				StartCoroutine ("FadeIn");				
			}
		} else {
			if(currentDisplayState == DisplayState.Visible && alreadyChecked == true)
			{
				StopCoroutine ("FadeIn");
				StartCoroutine ("FadeOut");	
			}
		}



	}

	IEnumerator FadeOut()
	{
	
		currentDisplayState = DisplayState.Changing;
		Debug.Log ("Trigger In");
		for (float i = 0; i <= 1f; i += 0.05f) {

			mainMod.startColor = new Color(i, i, i, i);
			canvasGroup.alpha = 1-i;
			yield return new WaitForEndOfFrame ();
		}

		//mainMod.startColor = new Color(1,1,1,1);
		canvasGroup.alpha = 0;

		currentDisplayState = DisplayState.Invisible;

		gameObject.SetActive (false);

	}

	IEnumerator FadeIn()
	{
		Debug.Log ("Trigger Out");

		currentDisplayState = DisplayState.Changing;
		
		for (float i = 1; i >= 0f; i -= 0.05f) {

			mainMod.startColor = new Color(i, i, i, i);


			canvasGroup.alpha = 1-i;
			yield return new WaitForEndOfFrame ();
		}


		mainMod.startColor = new Color(0,0,0,0);


		canvasGroup.alpha = 1f;
		if (triggerOnlyOnce) {
			alreadyChecked = true;
		}
		currentDisplayState = DisplayState.Visible;


	}



}
