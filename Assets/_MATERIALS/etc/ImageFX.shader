﻿Shader "Custom/ImageFX"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Vign("Vignette", 2D) = "white" {}
		_VignContrast("Vignette Contrast", Range(0,10)) = 0
		_AnimSpeed("Animation Speed", Range(0,1)) = 0
		_GameBrightness("Brightness", Range(-1,1)) = 0
		_VignAddColor("Vignette Color", Color) = (1,1,1,1)

		_NormalMap("Normal Map", 2D) = "white" {}

		_Dream("Dream Slider", Range(0,1)) = 0
		_DreamNormIntensity("Dream Normal Map Intensity", Range(0,0.2)) = 0
	}

	SubShader
	{
		// No culling or depth
		Cull Off 
		ZWrite Off 
		ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			// VARIABLES
			sampler2D _MainTex;
			sampler2D _Vign;
			fixed _VignContrast;
			half _AnimSpeed;
			sampler2D _NormalMap;
			fixed4 _VignAddColor;
			fixed _GameBrightness;
			uniform fixed _Dream;
			fixed _DreamNormIntensity;


			fixed4 frag (v2f i) : SV_Target
			{
				fixed2 uvPos = i.uv +  (_Time.y * _AnimSpeed); 
				fixed4 noiseColor = tex2D(_NormalMap, uvPos);

				//Get normal Screencolor
				fixed4 col = tex2D(_MainTex, i.uv +fixed2((noiseColor.x - 0.5)* _DreamNormIntensity * _Dream, (noiseColor.y - 0.5)* _DreamNormIntensity * _Dream));




				fixed4 colV = tex2D(_Vign, fixed2(i.uv.x + noiseColor.r/10 -noiseColor.g/10, i.uv.y - noiseColor.r/10 + noiseColor.g/10));

				colV = saturate( ( ( colV - 0.5 ) * _VignContrast ) + 0.5 );


				col = col* colV;

				col = (col + _Dream);

				col = saturate(col + _GameBrightness);

				return col;
			}
			ENDCG
		}
	}
}
