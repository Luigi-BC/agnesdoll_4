﻿Shader "Custom/Text_GLOW"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color("Text Color", Color) = (1,1,1,1)
		_NoiseMap("Noise Map", 2D) = "black" {}
		_AnimHeight("Animation Height", Range(0,50)) = 1
		_AnimSpeed("Animation Speed", Range(0,4)) = 1
	}
	SubShader
	{
		Tags { "Queue"="Transparent" }
		

		Blend SrcAlpha OneMinusSrcAlpha
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			fixed4 _Color;
			sampler2D _NoiseMap;
			half _AnimSpeed;
			half _AnimHeight;
									
			v2f vert (appdata v)
			{
				v2f o;

				fixed4 noiseVal = tex2D(_NoiseMap, v.uv + _Time.y * _AnimSpeed);

				o.vertex = UnityObjectToClipPos(v.vertex + float3(0, _AnimHeight *_AnimSpeed * unity_DeltaTime.z, 0));


				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{


				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);


				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);

				//_________________________________________________________

				col = col + _Color * col.a;



				return col;
			}


			ENDCG
		}
	}
}
